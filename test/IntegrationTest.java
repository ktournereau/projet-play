import org.junit.*;

import play.mvc.*;
import play.test.*;
import play.libs.F.*;

import static play.test.Helpers.*;
import static org.fest.assertions.Assertions.*;

import static org.fluentlenium.core.filter.FilterConstructor.*;

public class IntegrationTest {

    /**
     * add your integration test here
     * in this example we just check if the welcome page is being shown
     */
    @Test
    public void testHomePage() {
        running(testServer(3333, fakeApplication(inMemoryDatabase())), HTMLUNIT, new Callback<TestBrowser>() {
            public void invoke(TestBrowser browser) {
                browser.goTo("http://localhost:3333");

                // Affichage des banques
                assertThat(browser.pageSource()).contains("LCL");
                assertThat(browser.pageSource()).contains("HSBC");
                assertThat(browser.pageSource()).contains("Barclays");

                // Affichage des comptes
                assertThat(browser.pageSource()).contains("LCC00001");
                assertThat(browser.pageSource()).contains("LLA00002");

                assertThat(browser.pageSource()).contains("HCC00003");
                assertThat(browser.pageSource()).contains("HCJ00004");

                assertThat(browser.pageSource()).contains("BCC00005");
                assertThat(browser.pageSource()).contains("BLA00006");

                // Affichage des sommes
                assertThat(browser.pageSource()).contains("130.00 EUR");
                assertThat(browser.pageSource()).contains("3600.00 EUR");

                assertThat(browser.pageSource()).contains("760.00 USD");
                assertThat(browser.pageSource()).contains("1800.00 USD");

                assertThat(browser.pageSource()).contains("195.00 GBP");
                assertThat(browser.pageSource()).contains("2470.00 GBP");

                // Affichage du formulaire
                assertThat(browser.pageSource()).contains("Transfert");
                assertThat(browser.pageSource()).contains("Compte à débiter");
                assertThat(browser.pageSource()).contains("Compte à créditer");
                assertThat(browser.pageSource()).contains("Montant");
                assertThat(browser.pageSource()).contains("Devise");
                assertThat(browser.pageSource()).contains("Valider");

                // Affichage de l'historique
                assertThat(browser.pageSource()).contains("Historique");
            }
        });
    }

}
