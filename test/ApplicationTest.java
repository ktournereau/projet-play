import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.JsonNode;
import org.junit.*;

import play.mvc.*;
import play.test.*;
import play.data.DynamicForm;
import play.data.validation.ValidationError;
import play.data.validation.Constraints.RequiredValidator;
import play.i18n.Lang;
import play.libs.F;
import play.libs.F.*;

import static play.test.Helpers.*;
import static org.fest.assertions.Assertions.*;

import models.Bank;
import models.Currency;
import models.Euro;
import models.Dollar;
import models.LivreSterling;
import models.Account;
import models.Person;

/**
*
* Simple (JUnit) tests that can call all parts of a play app.
* If you are interested in mocking a whole application, see the wiki for more details.
*
*/
public class ApplicationTest {

    @Test
    public void testBankObject() {
        Euro euro = new Euro();
        Dollar dollar = new Dollar();
        LivreSterling livreSterling = new LivreSterling();

        Bank LCL = new Bank("LCL");
        Bank HSBC = new Bank("HSBC");

        assertThat(LCL.getName()).isEqualTo("LCL");
        assertThat(HSBC.getName()).isEqualTo("HSBC");

        Person bob = new Person("Bob", "LeBricoleur");
        Account account = new Account("12345AA", "Compte courant", bob, euro, 99.00);
        LCL.addAccount(account);
        assertThat(LCL.getAccount("12345AA")).isEqualTo(account);
       
        assertThat(LCL.getValue(euro)).isEqualTo(99.00);

        Account account2 = new Account("AA12345", "Compte jeune", bob, euro, 150.00);
        LCL.addAccount(account2);
        
        assertThat(LCL.getValue(euro)).isEqualTo(249.00);

    }

    @Test
    public void testCurrency() {
    	Currency currency = new Currency("EUR");
        Dollar dollar = new Dollar();

        assertThat(currency.getName()).isEqualTo("EUR");
        assertThat(currency.getValue()).isEqualTo(1.3658);
        assertThat(currency.to(dollar,1.00)).isEqualTo(1.3658);
    }

    @Test
    public void testEuro() {
        Euro euro = new Euro();
        Dollar dollar = new Dollar();
        LivreSterling livreSterling = new LivreSterling();

        assertThat(euro.getName()).isEqualTo("EUR");
        assertThat(euro.getValue()).isEqualTo(1.3658);

        // EUR -> USD
        assertThat(euro.to(dollar,1.00)).isEqualTo(1.3658);
        assertThat(euro.to(dollar,123.00)).isEqualTo(167.9934);
        assertThat(euro.to(dollar,987.00)).isEqualTo(1348.0446);

        // EUR -> EUR
        assertThat(euro.to(euro,1.00)).isEqualTo(1.00);
        assertThat(euro.to(euro,123.00)).isEqualTo(123.00);
        assertThat(euro.to(euro,987.00)).isEqualTo(987.00);

        // EUR -> GBP
        assertThat(euro.to(livreSterling,1.00)).isEqualTo(0.8234);
        assertThat(euro.to(livreSterling,123.00)).isEqualTo(101.2802);
        assertThat(euro.to(livreSterling,987.00)).isEqualTo(812.7115);
    }

    @Test
    public void testDollar() {
        Euro euro = new Euro();
        Dollar dollar = new Dollar();
        LivreSterling livreSterling = new LivreSterling();


        assertThat(dollar.getName()).isEqualTo("USD");
        assertThat(dollar.getValue()).isEqualTo(1.00);

        // USD -> EUR
        assertThat(dollar.to(euro,1.00)).isEqualTo(0.7322);
        assertThat(dollar.to(euro,123.00)).isEqualTo(90.0571);
        assertThat(dollar.to(euro,987.00)).isEqualTo(722.6534);

        //USD -> USD
        assertThat(dollar.to(dollar,1.00)).isEqualTo(1.00);
        assertThat(dollar.to(dollar,123.00)).isEqualTo(123.00);
        assertThat(dollar.to(dollar,987.00)).isEqualTo(987.00);
        
        //USD -> GBP
        assertThat(dollar.to(livreSterling,1.00)).isEqualTo(0.6029);
        assertThat(dollar.to(livreSterling,123.00)).isEqualTo(74.1545);
        assertThat(dollar.to(livreSterling,987.00)).isEqualTo(595.0443);

    }

    @Test
    public void testLivreSterling() {
        Euro euro = new Euro();
        Dollar dollar = new Dollar();
        LivreSterling livreSterling = new LivreSterling();

        assertThat(livreSterling.getName()).isEqualTo("GBP");
        assertThat(livreSterling.getValue()).isEqualTo(1.6587);

        // GBP -> GBP
        assertThat(livreSterling.to(livreSterling,1.00)).isEqualTo(1.00);
        assertThat(livreSterling.to(livreSterling,123.00)).isEqualTo(123.00);
        assertThat(livreSterling.to(livreSterling,987.00)).isEqualTo(987.00);

        // GBP -> EUR
        assertThat(livreSterling.to(euro,1.00)).isEqualTo(1.2145);
        assertThat(livreSterling.to(euro,123.00)).isEqualTo(149.3777);
        assertThat(livreSterling.to(euro,987.00)).isEqualTo(1198.6652);

        //GBP -> USD
        assertThat(livreSterling.to(dollar,1.00)).isEqualTo(1.6587);
        assertThat(livreSterling.to(dollar,123.00)).isEqualTo(204.0201);
        assertThat(livreSterling.to(dollar,987.00)).isEqualTo(1637.1369);
    }

    @Test
    public void testAccount(){
        Euro euro = new Euro();
        Person bob = new Person("Bob", "LeBricoleur");
        Account account = new Account("12345AA", "Compte courant", bob, euro, 99.00);

        assertThat(account.getId()).isEqualTo("12345AA");
        assertThat(account.getName()).isEqualTo("Compte courant");
        assertThat(bob.getFirstname()).isEqualTo("Bob");
        assertThat(bob.getLastname()).isEqualTo("LeBricoleur");
        assertThat(account.getCurrency().getName()).isEqualTo("EUR");
        assertThat(account.getValue(euro)).isEqualTo(99.00);
    }

    @Test
    public void testPerson(){
        Euro euro = new Euro();
        Dollar dollar = new Dollar();

        Bank LCL = new Bank("LCL");

        Person bob = new Person("Bob", "LeBricoleur");
        Account account = new Account("12345AA", "Compte courant", bob, euro, 99.00);
        Account account2 = new Account("AA12345", "Compte jeune",  bob, dollar, 1.00);

        LCL.addAccount(account);
        LCL.addAccount(account2);

        assertThat(bob.getFirstname()).isEqualTo("Bob");
        assertThat(bob.getLastname()).isEqualTo("LeBricoleur");
        assertThat(bob.getValue(euro)).isEqualTo(99.7322);
        assertThat(bob.getValue(dollar)).isEqualTo(136.2142);
    }

    @Test
    public void testTransfert(){
        Euro euro = new Euro();
        Dollar dollar = new Dollar();

        Bank LCL = new Bank("LCL");
        Bank HSBC = new Bank("HSBC");

        Person bob = new Person("Bob", "LeBricoleur");
        Person alice = new Person("Alice", "CaGlisse");

        Account account = new Account("12345AA", "Compte courant", bob, euro, 50.00);
        Account account2 = new Account("QWERTY", "Compte junior", bob, euro, 150.00);
        Account account3 = new Account("AZERTY", "Compte jeune", alice, dollar, 100.00);
        Account account4 = new Account("TOTTI", "Compte pourri", alice, dollar, 150.00);

        LCL.addAccount(account);
        LCL.addAccount(account2);
        HSBC.addAccount(account3);
        HSBC.addAccount(account4);
        
        assertThat(LCL.getValue(euro)).isEqualTo(200.00);
        assertThat(HSBC.getValue(dollar)).isEqualTo(250.00);

        account.send(account3, euro, 1.00);

        assertThat(LCL.getValue(euro)).isEqualTo(199.00);
        assertThat(HSBC.getValue(dollar)).isEqualTo(251.3658);

        assertThat(alice.getValue(dollar)).isEqualTo(251.3658);
        assertThat(bob.getValue(euro)).isEqualTo(199.00);

        assertThat(account3.getValue(dollar)).isEqualTo(101.3658);
        assertThat(account.getValue(euro)).isEqualTo(49.00);
    }
}

