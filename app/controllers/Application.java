package controllers;

import play.*;
import play.mvc.*;

import views.html.*;

import java.util.ArrayList;
import java.util.Map;

import models.*;

public class Application extends Controller {

    public final static ArrayList<Bank> banks = new ArrayList<Bank>();
    public final static ArrayList<Currency> currencys = new ArrayList<Currency>();
    public final static ArrayList<String> history = new ArrayList<String>();

    public final static Euro euro = new Euro();
    public final static Dollar dollar = new Dollar();
    public final static LivreSterling livreSterling = new LivreSterling();

    public static Result index() {

        // Initialisation des banques
        if(banks.size() == 0 || currencys.size() == 0) init();

        return ok(index.render(banks, currencys, "", history));
    }

    public static Result submitTransfert() {

        Map<String, String[]> values = request().body().asFormUrlEncoded();

        String from = values.get("from")[0];
        String to = values.get("to")[0];
        String amount = values.get("amount")[0];
        String currencyName = values.get("currency")[0];

        Account fromAccount = null;
        Account toAccount = null;
        Currency currency = null;

        for(Currency c: currencys){
            if(c.getName().equals(currencyName)) currency = c;
        }

        for(Bank b: banks){
            if(b.getAccount(from) != null) fromAccount = b.getAccount(from);
            if(b.getAccount(to) != null) toAccount = b.getAccount(to);
        }

        try{
            fromAccount.send(toAccount, currency, Double.parseDouble(amount));
            history.add(""+Double.parseDouble(amount)+" "+currency.getName()+" : "+fromAccount.getId()+" -> "+toAccount.getId());
            return ok(index.render(banks, currencys, "", history));
        }catch(Exception e){
            return ok(index.render(banks, currencys, "Une erreur c'est produite !", history));
        }
    }

    private static void init(){

        currencys.add(euro);
        currencys.add(dollar);
        currencys.add(livreSterling);


        Bank lcl = new Bank("LCL");
        banks.add(lcl);

        Bank hsbc = new Bank("HSBC");
        banks.add(hsbc);

        Bank barclays = new Bank("Barclays");
        banks.add(barclays);

        Person anne = new Person("Anne", "Onnyme");
        Person sansy = new Person("Sansy", "Dentitey");
        Person jean = new Person("Jean", "Neymar");

        Account account1 = new Account("LCC00001", "Compte Courant", anne, euro, 130.00);
        Account account2 = new Account("LLA00002", "Livret A", anne, euro, 3600.00);
        
        Account account3 = new Account("HCC00003", "Compte Courant", sansy, dollar, 760.00);
        Account account4 = new Account("HCJ00004", "Compte Jeune", sansy, dollar, 1800.00);

        Account account5 = new Account("BCC00005", "Compte Courant", jean, livreSterling, 195.00);
        Account account6 = new Account("BLA00006", "Livret A", jean, livreSterling, 2470.00);

        lcl.addAccount(account1);
        lcl.addAccount(account2);

        hsbc.addAccount(account3);
        hsbc.addAccount(account4);

        barclays.addAccount(account5);
        barclays.addAccount(account6);

    }

}
