package models;

import java.util.*;
import javax.validation.*;
import play.data.validation.Constraints.*;

public class Dollar extends Currency {

	/**
	 * Constructeur de la classe Dollar
	 */
    public Dollar() {
        super("USD");
    }

}
