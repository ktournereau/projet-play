package models;

import java.util.*;
import javax.validation.*;
import play.data.validation.Constraints.*;

public class Account {

	private String id;             // Identifiant du compte
	private String name;           // Libelle du compte
	private Person person;         // Proprietaire du compte
	private Currency currency;     // Devise du compte
	private double value;          // Solde du compte


    /**
     * Constructeur de la classe Account
     *
     * @param String id Identifiant du compte
     * @param String name Nom du compte
     * @param Person person Propriétaire du compte
     * @param Currency currency Devise du compte
     * @param double value Solde du compte
     */
    public Account(String id, String name, Person person, Currency currency, double value) {
    	this.id = id;
    	this.name = name;
    	this.person = person;
    	this.currency = currency;
    	this.value = value;
    }

    /**
     * Methode Send
     * Permet l'envoi d'argent entre 2 comptes
     *
     * @param Account account Compte destinataire
     * @param Currence currency Devise du transfert
     * @param double value Montant du transfert
     *
     * @return void
     */
    public void send(Account account, Currency currency, double value){
        this.value = this.value - currency.to(this.currency, value);
        //account.value = account.value + currency.to(account.currency.name, value);

        double accountValue = account.getValue(account.getCurrency()) + currency.to(account.getCurrency(), value);
        account.setValue(account.getCurrency(), accountValue);
    }

    /**
     * GetId
     *
     * @return String Identifiant du compte
     */ 
    public String getId(){
        return this.id;
    }

    /**
     * SetId
     *
     * @param String id Identifiant du compte
     * @return void
     */
    public void setId(String id){
        this.id = id;
    }

    /**
     * GetName
     *
     * @return String name Nom du compte
     */
    public String getName(){
        return this.name;
    }

    /**
     * SetName
     *
     * @param String name Nom du compte
     * @return void
     */
    public void setName(String name){
        this.name = name;
    }

    /**
     * GetPerson
     *
     * @return Person person Propriétaire du compte
     */
    public Person getPerson(){
        return this.person;
    }

    /**
     * SetPerson
     *
     * @param Person person Propriétaire du compte
     * @return void
     */
    public void setPerson(Person person){
        this.person = person;
    }

    /**
     * GetCurrency
     *
     * @return Currency currency Devise du compte
     */
    public Currency getCurrency(){
        return this.currency;
    }

    /**
     * SetCurrency
     *
     * @param Currency currency Devise du compte
     * @return void
     */
    public void setCurrency(Currency currency){
        this.currency = currency;
    }

    /**
     * GetValue
     *
     * @param Currency currency Devise souhaitée
     * @return double value Valeur du compte dans la devise souhaitée
     */
    public double getValue(Currency currency){
        return this.currency.to(currency, this.value);
    }

    /**
     * SetValue
     *
     * @param Currency currency Devise de la valeur passée
     * @param double value Valeur du compte
     * @return void
     */
    public void setValue(Currency currency, double value){
        this.value = currency.to(this.currency, value);
    }

}
