package models;

import java.util.*;
import javax.validation.*;
import play.data.validation.Constraints.*;

public class Bank {
    private String name;                                                        // Nom de la banque
    private HashMap<String,Account> accountMap = new HashMap<String,Account>(); // Liste des compte de la banque
    
    /**
     * Constructeur de la classe Bank
     *
     * @param String name Nom de la banque
     */
    public Bank(String name) {
	   this.name = name;
    }

    /**
     * Methode addAccount
     * Ajoute un compte à la banque
     *
     * @param Account account Compte à ajouter
     * @return void
     */
    public void addAccount(Account account){

        // Ajout du compte seulement s'il n'éxiste pas
        if (this.accountMap.get(account.getId()) == null) {
            this.accountMap.put(account.getId(), account);
            account.getPerson().addAccount(account);
        }
    }

    /**
     * Methode getAccount
     * Retourne un compte en fonction de son id
     *
     * @param String id
     * @return Account account Compte
     */
    public Account getAccount(String id){
        return this.accountMap.get(id);
    }

    /**
     * Methode getAccounts
     * Retourne la liste des comptes
     *
     * @return ArrayList<Account> accounts Comptes
     */
    public ArrayList<Account> getAccounts(){
        ArrayList<Account> accounts = new ArrayList<Account>();

        for(String currentKey : this.accountMap.keySet()){
            accounts.add(this.accountMap.get(currentKey));
        }

        return accounts;
    }

    /**
     * Methode getValue
     * Retourne le solde de la banque (somme de tous les comptes)
     *
     * @param Currency currency Devise souhaitée
     * @return double value Solde de la banque
     */
    public double getValue(Currency currency){
        double value = 0;

        for(String currentKey : this.accountMap.keySet()){
            value = value + this.accountMap.get(currentKey).getValue(currency);
        }
        return (double)(Math.round(value * 10000)) / 10000;
    }

    /**
     * GetName
     *
     * @return String name Nom de la banque
     */
    public String getName(){
        return this.name;
    }

    /**
     * SetName
     *
     * @param String name Nom de la banque
     * @return void
     */
    public void setName(String name){
        this.name = name;
    }
}
