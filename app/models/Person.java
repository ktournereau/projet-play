package models;

import java.util.*;
import javax.validation.*;
import play.data.validation.Constraints.*;

public class Person {
    private String firstname;                                                       // Prénom
    private String lastname;                                                        // Nom
    private HashMap<String,Account> accountMap = new HashMap<String,Account>();     // Liste des comptes
    
    /**
     * Constructeur de la classe Person
     *
     * @param String firstname Prénom
     * @param String lastname Nom
     */
    public Person(String firstname, String lastname) {
	   this.firstname = firstname;
	   this.lastname = lastname;
    }

    /**
     * Methode addAccount
     * Ajoute un compte
     *
     * @param Account account Compte à ajouter
     * @return void
     */
    public void addAccount(Account account){
        if (this.accountMap.get(account.getId()) == null) {
            this.accountMap.put(account.getId(), account);
        }
    }

    /**
     * Methode getValue
     * Retourne la somme des comptes de la personne
     *
     * @param Currency currency Devise souhaitée
     * @return double value Solde
     */
    public double getValue(Currency currency){
        double value = 0;

        for(String currentKey : this.accountMap.keySet()){
            value = value + this.accountMap.get(currentKey).getValue(currency);
        }
        return (double)(Math.round(value * 10000)) / 10000;
    }

    /**
     * GetFirstname
     *
     * @return String firstname Prénom
     */
    public String getFirstname(){
        return this.firstname;
    }

    /**
     * SetFirstname
     *
     * @param String firstname Prénom
     * @return void
     */
    public void setFirstname(String firstname){
        this.firstname = firstname;
    }

    /**
     * GetLastname
     *
     * @return String lastname Nom
     */
    public String getLastname(){
        return this.lastname;
    }

    /**
     * SetLastname
     *
     * @param String lastname Nom
     * @return void
     */
    public void setLastname(String lastname){
        this.lastname = lastname;
    }
}
