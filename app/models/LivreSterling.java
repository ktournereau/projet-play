package models;

import java.util.*;
import javax.validation.*;
import play.data.validation.Constraints.*;

public class LivreSterling extends Currency {

	/**
	 * Constructeur de la classe LivreSterling
	 */
    public LivreSterling() {
        super("GBP");
    }

}
