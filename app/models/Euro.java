package models;

import java.util.*;
import javax.validation.*;
import play.data.validation.Constraints.*;

public class Euro extends Currency {

	/**
	 * Constructeur de la classe Euro
	 */
    public Euro() {
        super("EUR");
    }

}
