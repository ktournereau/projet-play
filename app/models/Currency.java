package models;

import java.util.*;
import javax.validation.*;
import play.data.validation.Constraints.*;

public class Currency {

    private String name;                                                        // Nom de la devise
    private HashMap<String,Double> valuesMap = new HashMap<String,Double>();    // Tableau de correspondance Devise => USD
    
    /**
     * Constructeur de la classe Currency
     *
     * @param String currency Nom de la devise
     */
    public Currency(String currency) {

        // Set values map ----- Valeur Google au 28/01/14
        this.valuesMap.put("USD",1.00);
        this.valuesMap.put("EUR",1.3658);
        this.valuesMap.put("GBP",1.6587);

        this.name = currency;
    }

    /**
     * Methode to
     * Retourne la conversion d'une valeur dans la devise souhaitée
     *
     * @param Currency currency Devise souhaitée
     * @param double amount Valeur à convertir
     *
     * @return double value Valeur
     */
    public double to(Currency currency, double amount){

        // Convert Currency to USD
        double toUSD = amount * this.getValue();

        // Convert USD to final Currency
        double result = toUSD / this.valuesMap.get(currency.getName());
        
        // return format #,####
    	return (double)(Math.round(result * 10000)) / 10000;
    }

    /**
     * GetName
     *
     * @return String name Nom de la devise
     */
    public String getName(){
        return this.name;
    }

    /**
     * SetName
     *
     * @param String name Nom de la devise
     * @return void
     */
    public void setName(String name){
        this.name = name;
    }

    /**
     * GetValue
     *
     * @return double value Valeur en USD
     */
    public double getValue(){
        return this.valuesMap.get(this.name);
    }
}
